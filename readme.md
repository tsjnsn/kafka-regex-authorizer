# Usage

Prefix the ACL principal (after `User:`) with `~/` to use a regex.

# Configuring

Copy the jar to each broker's filesystem and add to the classpath.

Set `authorizer.class.name=kafka.security.auth.RegexAclAuthorizer` on all the brokers.

# Example

`User:~/test-.*`
